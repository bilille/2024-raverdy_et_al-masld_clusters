
# Function to perform the enrichment analysis
enrichMsigdbFct = function(results, dataBase, symbEntrezid,
                           seuilAdjP = 0.05, seuilLogFC = 0.26, seuilEnrch = 0.05){
  
  ## Prepare data
  # Recup entrezid
  dataEnrcht = merge(results, symbEntrezid, by.x = 0, by.y = "SYMBOL", all.x = TRUE)
  names(dataEnrcht)[1] = "SYMBOL"
  
  # Compter combien on en perd d'importants
  nLose = nrow(dataEnrcht[is.na(dataEnrcht$ENTREZID) & dataEnrcht$adj.P.Val < seuilAdjP& abs(dataEnrcht$logFC) > seuilLogFC,])
  nSign = nrow(dataEnrcht[dataEnrcht$adj.P.Val < seuilAdjP& abs(dataEnrcht$logFC) > seuilLogFC,])
  # cat("\nAs information,", paste0("**", nLose, "**"),"of the", paste0("**", nSign, "**"), "DEGs don't match for ENTREZID.\n")
  
  ## Create vector
  genesSorted = abs(dataEnrcht[!is.na(dataEnrcht$ENTREZID), "t"])
  names(genesSorted) = dataEnrcht[!is.na(dataEnrcht$ENTREZID), "ENTREZID"]
  genesSorted = sort(genesSorted, decreasing = TRUE)
  
  ## Perform enrichment
  set.seed(24)
  gseaEnrich = GSEA(genesSorted, TERM2GENE = dataBase,
                    minGSSize = 30, maxGSSize = 500, pvalueCutoff = seuilEnrch,
                    scoreType = "pos", verbose = FALSE)
  
  ## Récupérer la table de résultat
  resultEnrich = gseaEnrich@result
  resultHist = resultEnrich[1:15, c("Description", "p.adjust", "pvalue")]
  resultHist$col = ifelse(resultHist$p.adjust < 0.05, "0",
                          ifelse(resultHist$pvalue < 0.05, "1", "2"))
  # Alléger les étiquettes
  resultHist$cat = substr(resultHist$Description, 1, 35)
  
  return(list(resultHist = resultHist, resultEnrich = resultEnrich))
}


# Function to plot the enrichment results (histograms)
printResultsEnrchHistFct = function(enrchResults, xlab, color = c("darkblue", "cyan")){
  
  ggplot(data = enrchResults, aes(x=reorder(cat, -pvalue), y=-log10(pvalue), fill = col)) +
    geom_bar(stat = "identity") + coord_flip() +
    
    theme(
      legend.position = "none",
      axis.text = element_text(size = 14),
      axis.title = element_text(size = 18)
    ) +
    xlab(xlab) +
    scale_fill_manual(values = color)
}


