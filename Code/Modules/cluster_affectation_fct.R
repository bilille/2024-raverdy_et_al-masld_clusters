#-------------------------------------------------------------------------------
# clustAffFct
#-------------------------------------------------------------------------------

# The clustAffFct function assigns the validation data individuals to their appropriate clusters using the results of the PAM model calculated on the ABOS training data.

# dataToAff : Table containing the clinical variables


clustAffFct = function(dataToAff){
  
  # Calculate distance to all centers
  distMatrix = data.frame(distClust1 = sqrt((dataToAff$AgeJourIntervention-0.8788154)^2 + (dataToAff$BMI-(-0.06122751))^2 + (dataToAff$hba1cpc-(-0.22208015))^2 + (dataToAff$TGPUL-(-0.1471468))^2 + (dataToAff$triglymmolL-(-0.10692422))^2 + (dataToAff$cholest_LDLmmolL-(-0.8744864))^2),
                          distClust2 = sqrt((dataToAff$AgeJourIntervention-1.0501193)^2 + (dataToAff$BMI-(-0.08665909))^2 + (dataToAff$hba1cpc-1.52742507)^2 + (dataToAff$TGPUL-0.8440430)^2 + (dataToAff$triglymmolL-0.76260098)^2 + (dataToAff$cholest_LDLmmolL-(-0.9340297))^2),
                          distClust3 = sqrt((dataToAff$AgeJourIntervention-(-0.7485709))^2 + (dataToAff$BMI-1.71898277)^2 + (dataToAff$hba1cpc-(-0.57198120))^2 + (dataToAff$TGPUL-(-0.3724172))^2 + (dataToAff$triglymmolL-(-0.23536631))^2 + (dataToAff$cholest_LDLmmolL-(-0.2223564))^2),
                          distClust4 = sqrt((dataToAff$AgeJourIntervention-0.3649040)^2 + (dataToAff$BMI-(-0.21381697))^2 + (dataToAff$hba1cpc-(-0.36204057))^2 + (dataToAff$TGPUL-(-0.2372549))^2 + (dataToAff$triglymmolL-(-0.05055755))^2 + (dataToAff$cholest_LDLmmolL-1.0942392)^2),
                          distClust5 = sqrt((dataToAff$AgeJourIntervention-(-0.2346594))^2 + (dataToAff$BMI-(-0.29011169))^2 + (dataToAff$hba1cpc-(-0.08211973))^2 + (dataToAff$TGPUL-2.2407195)^2 + (dataToAff$triglymmolL-0.09728946)^2 + (dataToAff$cholest_LDLmmolL-0.3944271)^2),
                          distClust6 = sqrt((dataToAff$AgeJourIntervention-(-1.0055267))^2 + (dataToAff$BMI-(-0.32825905))^2 + (dataToAff$hba1cpc-(-0.64196140))^2 + (dataToAff$TGPUL-(-0.4625253))^2 + (dataToAff$triglymmolL-(-0.33701112))^2 + (dataToAff$cholest_LDLmmolL-(-0.3765523))^2),
                          Id = row.names(dataToAff)
  )
  
  # Identify the minimum
  distMatrix$min = unlist(apply(distMatrix, 1, function(r) min(r[1:6])))
  # And the associated cluster
  distMatrix$clust = unlist(apply(distMatrix, 1, function(r) which(r[1:6] == r[8])))
  
  # Data to return = Id + cluster
  clustAff <- distMatrix[,c("Id", "clust")]
  
  # Gather non enriched clusters into CTRL cluster
  clustAff$classeGath  <- ifelse(clustAff$clust == 2, "CM",
                                 ifelse(clustAff$clust == 5, "LS", "CTRL"))
  
  # Return
  return(clustAff)
}

#-------------------------------------------------------------------------------