
#-------------------------------------------------------------------------------
### Functions to perform differential analyses and print the results
## Very specific, not adapted to a general case
#-------------------------------------------------------------------------------


comparisonsLimmaFct = function(fitLM, contr, design, nbGenes){
  
  # Contrast
  # cat("Comparison :", contr)
  contr <<- contr
  contrastMatrix = makeContrasts(contr, levels = design)
  
  # Comparaison
  fit2 = contrasts.fit(fitLM, contrastMatrix)
  
  # Variance correction
  fit2 = eBayes(fit2)
  
  # p-value adjustment
  mstat = topTable(fit2, coef = 1, number = nbGenes)
  mstat <- mstat[order(mstat$adj.P.Val),]
  
  return(list(mstat = mstat, contr = contr))
}


printResultsLimmaFct = function(resultComp, seuilAdjP = 0.05, seuilLogFC = 0.26,
                                topPrintHist = TRUE, topPrintVolc = TRUE,
                                color = c("blue", "grey", "red"), legendPos = "right", 
                                topMetabo = FALSE){
  
  listMetabo = c("glucose", "1,5-anhydroglucitol (1,5-AG)", "4-hydroxyphenylacetylglutamine", "p-cresol glucuronide*", "4-hydroxyphenylacetate", "homocitrulline", "imidazole propionate", "deoxycholate", "5alpha-androstan-3alpha,17alpha-diol monosulfate", "5alpha-androstan-3alpha,17beta-diol disulfate", "5alpha-androstan-3alpha,17beta-diol monosulfate (1)", "5alpha-androstan-3alpha,17beta-diol monosulfate (2)", "glycoursodeoxycholic acid sulfate (1)", "taurochenodeoxycholic acid 3-sulfate", "ursodeoxycholate", "glycochenodeoxycholate", "glycochenodeoxycholate glucuronide (1)", "glycochenodeoxycholate 3-sulfate", "cysteine-glutathione disulfide")
  
  results = resultComp[["mstat"]]
  results = results[order(results$adj.P.Val),]
  
  ## Number of differentially expressed genes
  nSign = length(which(results$adj.P.Val<=seuilAdjP))
  nSignFC = length(which(results$adj.P.Val<=seuilAdjP & abs(results$logFC) >= seuilLogFC))
  
  cat("\nThere are", nSign, "differentially expressed features considering threshold", seuilAdjP, "on adjusted P-values, and", nSignFC, "when adding the threshold", seuilLogFC, "on absolute value of log2 Fold Change.\n")
  
  ## Compute the corresponding threshold on raw p-value
  if (nSign > 0){
    seuilpval = -log10(results$P.Value[nSign])
  } else {
    seuilpval = -log10(seuilAdjP/nrow(results))
  }
  
  ## labels to print : 20 first up and down regulated genes
  results$name = ""
  cptPos = 0
  cptNeg = 0
  cpt = 1
  while ((cptPos < 20 | cptNeg <20) & results[cpt,"adj.P.Val"] < seuilAdjP & cpt < nrow(results)){
    if (results[cpt,"logFC"] < -seuilLogFC & cptNeg < 20){
      results[cpt, "name"] = ifelse(!topMetabo, row.names(results)[cpt],
                                    ifelse(results[cpt, "CHEMICAL_NAME"] %in% listMetabo, results[cpt, "CHEMICAL_NAME"], ""))
      cptNeg = cptNeg + 1
    } else if (results[cpt,"logFC"] > seuilLogFC & cptPos < 20){
      results[cpt, "name"] = ifelse(!topMetabo, row.names(results)[cpt],
                                    ifelse(results[cpt, "CHEMICAL_NAME"] %in% listMetabo, results[cpt, "CHEMICAL_NAME"], ""))
      cptPos = cptPos + 1
    }
    cpt = cpt + 1
  }
  
  ## Legend elements
  results$OverExpression = ifelse(results$logFC > seuilLogFC & results$adj.P.Val < seuilAdjP, 
                                  paste("in", str_split(resultComp[["contr"]], "-", simplify = TRUE)[1]),
                                  ifelse(results$logFC < -seuilLogFC & results$adj.P.Val < seuilAdjP, 
                                         paste("in", str_split(resultComp[["contr"]], "-", simplify = TRUE)[2]), "Not DE"))
  
  ## Raw p-values histogram
  if (topPrintHist) hist(results$P.Value, main = 'Histogram of raw p-values', xlab = 'p-values')
  
  ## Volcano plot
  p = ggplot(results, aes(x = logFC, y = -log10(P.Value), col = OverExpression)) +
    geom_point() +
    
    # Seuil pval
    geom_hline(yintercept = seuilpval) +
    
    # Seuil logFC
    geom_vline(xintercept = seuilLogFC) +
    geom_vline(xintercept = -seuilLogFC) +
    
    # Couleurs
    scale_color_manual(values = color) +  
    theme_bw() +
    
    # Etiquettes
    geom_label_repel(data = subset(results, name != ""),
                     aes(label = name),
                     fontface = "bold.italic",
                     size = 5,
                     max.overlaps = 8) +
    
    # Légende
    xlab(label = "log2FoldChange") +
    
    theme(axis.text = element_text(size = 18),
          axis.title = element_text(size = 20),
          legend.position = legendPos)
  
  if (topPrintVolc) print(p)
  
  ## Table
  results$logFC = round(results$logFC, 5)
  results$adj.P.Val = round(results$adj.P.Val, 5)
  if (topMetabo){
    results = results[results$adj.P.Val < seuilAdjP & abs(results$logFC) >= seuilLogFC, c("CHEMICAL_NAME", "logFC", "adj.P.Val")]
  } else {
    results = results[results$adj.P.Val < seuilAdjP & abs(results$logFC) >= seuilLogFC, c("logFC", "adj.P.Val")]
  }
  
  return(list(results = results, volcano = p))
}


#-------------------------------------------------------------------------------